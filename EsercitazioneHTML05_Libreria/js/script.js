//=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=
// INIZIALIZZAZIONE VARIABILI
//=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=
let libreria = [];

//=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=
// INSERIMENTO (Create)
//=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=
function inserisci(){

    if(campiVuoti())
    {
        alert("Riempire tutti i campi!");
        return;
    }

    let _titolo = document.getElementById("input_titolo").value;
    let _autore = document.getElementById("input_autore").value;
    let _codice = document.getElementById("input_codice").value;
    let _prezzo = document.getElementById("input_prezzo").value;
    let _quant = document.getElementById("input_quant").value;

    let libro = {
        id: calcolaId(),
        titolo: _titolo,
        autore: _autore,
        codice: _codice,
        prezzo: _prezzo,
        quant: _quant,
    }

    if(!bookExist(libro))
        libreria.push(libro)
    // Nel caso esiste, l'incremento viene fatto nella funzione stessa
    
    svuotaCampi();

    localStorage.setItem("Libreria", JSON.stringify(libreria));

    mostraLibreria();
}

function calcolaId(){
    if(libreria.length != 0)
        return libreria[libreria.length - 1].id + 1;

    return 1;
}

function bookExist(_libro){
    for(let [index, book] of libreria.entries()){
        if(_libro.codice == book.codice)
        {
            book.quant += _libro.quant;
            return true;
        }
    }
    return false;
}

//=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=
// Operazioni sui campi di input
//=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=
function campiVuoti(){
    let _titolo = document.getElementById("input_titolo").value;
    let _autore = document.getElementById("input_autore").value;
    let _codice = document.getElementById("input_codice").value;
    let _prezzo = document.getElementById("input_prezzo").value;
    let _quant = document.getElementById("input_quant").value;

    if(_titolo == "" || _autore == "" || _codice == "" || _prezzo == 0 || _quant == 0)
        return true;
    else
        return false;
}

function svuotaCampi(){
    document.getElementById("input_titolo").value = "";
    document.getElementById("input_autore").value = "";
    document.getElementById("input_codice").value = "";
    document.getElementById("input_prezzo").value = "";
    document.getElementById("input_quant").value = "";
    document.getElementById("input_titolo").focus();
}

//=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=
// ELIMINAZIONE (Delete)
//=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=
function rimuoviLibro(var_id){
    for(let [index, book] of libreria.entries())
    {
        if(book.id == var_id)
        {
            libreria.splice(index, 1)
        } 
    };
    localStorage.setItem("Libreria", JSON.stringify(libreria));
    mostraLibreria();
}

function svuotaLibreria(){
    if(libreria.length != 0)
    {
        libreria.length = 0;
        localStorage.setItem("Libreria", JSON.stringify(libreria));
        mostraLibreria();
        alert("Libreria svuotata!");
    }
    else
    {
        alert("La libreria è già vuota.")
    }
}

//=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=
// MODIFICA LIBRO (Update)
//=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=
function modificaLibro(){
    alert("Funzione non ancora implementata");
}

//=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=
// MOSTRA LIBRERIA (Read)
//=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=
function mostraLibreria(){
    let sezione_html = "";

    for(let [index, book] of libreria.entries())
    {
        sezione_html += `
        <tr>
            <td>${book.titolo}</td>
            <td>${book.autore}</td>
            <td>${book.codice}</td>
            <td>${book.prezzo}</td>
            <td>${book.quant}</td>
            <td>
                <button class="btn btn-outline-info" onclick="modificaLibro()">
                    <i class="fa-solid fa-pen-to-square"></i>
                </button>
                <button class="btn btn-outline-danger" onclick="rimuoviLibro(${book.id})">
                    <i class="fa-solid fa-trash-can"></i>
                </button>
            </td>
        </tr>
        `
    }
    document.getElementById("valori-tab").innerHTML = sezione_html;
}

//=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=
// IMPOSTA LA LIBRERIA NEL LOCALSTORAGE
//=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=
if(localStorage.getItem("Libreria") == null)
    localStorage.setItem("Libreria", JSON.stringify([]));

//=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=
// PRENDI I DATI DAL LOCALSTORAGE
//=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=
libreria = JSON.parse(localStorage.getItem("Libreria"));

//=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=
// MOSTRA VALORI TAB SOLO SE GIA' PRESENTI (altrimenti va in errore)
//=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_=
if(document.getElementById("valori-tab") != null)
    mostraLibreria();