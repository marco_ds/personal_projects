﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;


namespace MONGO_01_eCommerce.Models
{
    public class Category
    {
        // Primary Key
        [BsonId]
        public ObjectId DocumentID { get; set; }

        [Required]
        [MaxLength(32)]
        public string Name { get; set; }

        [Required]
        [MaxLength(512)]
        public string Description { get; set; }

        [Required]
        [StringLength(64)]
        public string Code { get; set; }

        [Required]
        [MaxLength(8)]
        public string Shelf { get; set; }
    }
}
