﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;


namespace MONGO_01_eCommerce.Models
{
    public class Product
    {
        [BsonId]
        public ObjectId DocumentID { get; set; }

        [MaxLength(32)]
        [Required]
        public string Name { get; set; }

        [MaxLength(512)]
        [Required]
        public string Description { get; set; }

        [Required]
        public int Price { get; set; }

        [Required]
        public int Quantity { get; set; }

        [MaxLength(64)]
        [Required]
        public string ProductCode { get; set; }

        // Generato automaticamente
        [StringLength(64)]
        public string Code { get; set; }
        
        // CATEGORY
    }
}
