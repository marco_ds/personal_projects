﻿using MONGO_01_eCommerce.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MONGO_01_eCommerce.DAL
{
    public class CategoryRepo : InterfaceRepo<Category>
    {
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
        //                               CONNECTION                              //
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//

        private IMongoCollection<Category> categories;
        public CategoryRepo(string connectionString, string databaseString)
        {
            var client = new MongoClient(connectionString);

            var database = client.GetDatabase(databaseString);

            if (categories == null)
                categories = database.GetCollection<Category>("Categories");
        }

        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
        //                                  CRUD                                 //
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//

        // INSERT
        public bool Insert(Category t)
        {
            
            Category temp = categories.Find(d => d.Code == t.Code).FirstOrDefault();

            if (temp == null)
            {
                categories.InsertOne(t);
                if (t.DocumentID != null)
                    return true;
            }
            return false;
        }

        // READ
        public IEnumerable<Category> GetAll()
        {
            throw new NotImplementedException();
        }

        public Category GetById(ObjectId varId)
        {
            throw new NotImplementedException();
        }

        // UPDATE
        public bool Update(Category t)
        {
            throw new NotImplementedException();
        }

        // DELETE
        public bool Delete(Category t)
        {
            throw new NotImplementedException();
        }
    }
}
