﻿using MONGO_01_eCommerce.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MONGO_01_eCommerce.DAL
{
    public class ProductRepo : InterfaceRepo<Product>
    {
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
        //                               CONNECTION                              //
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//

        private IMongoCollection<Product> products;
        public ProductRepo(string connectionString, string databaseString)
        {
            var client = new MongoClient(connectionString);

            var database = client.GetDatabase(databaseString);

            if (products == null)
                products = database.GetCollection<Product>("Products");
        }

        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
        //                                  CRUD                                 //
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//

        // INSERT
        public bool Insert(Product t)
        {

            products.InsertOne(t);

            if (t.DocumentID != null)
                return true;

            return false;
        }

        // READ
        public IEnumerable<Product> GetAll()
        {
            throw new NotImplementedException();
        }

        public Product GetById(ObjectId varId)
        {
            throw new NotImplementedException();
        }

        // UPDATE
        public bool Update(Product t)
        {
            throw new NotImplementedException();
        }

        // DELETE
        public bool Delete(Product t)
        {
            throw new NotImplementedException();
        }
    }
}
