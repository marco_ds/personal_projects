﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MONGO_01_eCommerce.DAL
{
    interface InterfaceRepo<T>
    {
        // INSERT INTERFACE
        bool Insert(T t);

        // READ INTERFACE
        IEnumerable<T> GetAll();
        T GetById(ObjectId varId);

        // UPDATE INTERFACE
        bool Update(T t);

        // DELETE INTERFACE
        bool Delete(T t);
    }
}
