﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MONGO_01_eCommerce.DAL;
using MONGO_01_eCommerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MONGO_01_eCommerce.Controllers
{
    [ApiController]
    [Route("api/products")]
    public class ProductController : Controller
    {
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
        //                               CONNECTION                              //
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//

        private readonly ProductRepo _repo;
        public ProductController(IConfiguration config)
        {
            if (_repo == null)
            {
                // switch per cambiare tra db locale e remoto
                bool isLocal = config.GetValue<bool>("isLocal");
                // controllo e passaggio della stringa di connessione
                string connectionString = isLocal == true ?
                    config.GetValue<string>("MongoSettings:LocalDb") :
                    config.GetValue<string>("MongoSettings:RemoteDb");
                // passaggio del nome del database
                string databaseString = config.GetValue<string>("MongoSettings:DbName");
                // crea la repo del prodotto passando le due variabili appena create
                _repo = new ProductRepo(connectionString, databaseString);
            }
        }

        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
        //                                  CRUD                                 //
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//

        // CREATE
        [HttpPost("insert")]
        public ActionResult Insert(Product category)
        {
            if (_repo.Insert(category))
                return Ok(new { Status = "Success", Descrizione = "" });

            return Ok(new { Status = "Error", Descrizione = "Errore nel caricamento del documento" });
        }

        // READ

        // UPDATE

        // DELETE

    }
}
