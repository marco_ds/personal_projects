/* ---- Jquery ---- */ 
function sect_insert() {
    $('#ins').show();
    $('#mod').hide();
}

function sect_modify() {
    $('#ins').hide();
    $('#mod').show();
}

/* JavaScript */
let libreria = [
{
        id: 1,
        titolo: "Meaningless",
        artista: "Pain of Salvation",
        anno: 2017,
    },
    {
        id: 2,
        titolo: "Entombed",
        artista: "Deftones",
        anno: 2012,
    },
    {
        id: 3,
        titolo: "Sleepwalking",
        artista: "Bring Me The Horizon",
        anno: 2013,
    },
    {
        id: 4,
        titolo: "Juno",
        artista: "TesseracT",
        anno: 2018,
    },
];

// INSERISCI CANZONE
function inserisci(){
    let var_titolo = document.getElementById("inp_titolo").value;
    let var_artista = document.getElementById("inp_artista").value;
    let var_anno = document.getElementById("inp_anno").value;

    if(var_titolo == 0 || var_artista == 0 || var_anno == 0)
    {
        alert("Riempi tutti i campi!");
        return;
    }

    let canzone = {
        id: calcolaId(),
        titolo: var_titolo,
        artista: var_artista,
        anno: var_anno,
    }

    let isDuplicate;

    for(let [index, song] of libreria.entries())
    {
        if(var_titolo == song.titolo && var_artista == song.artista && var_anno == song.anno)
        {
            alert("Canzone già nella libreria!");
            return;
        }
    }

    libreria.push(canzone);

    mostraLibreria();
    svuotaCampi();
    alert("Inserimento completato!");
}

// ID INCREMENTALE
function calcolaId(){
    if(libreria.length != 0)
        return libreria[libreria.length - 1].id + 1;

    return 1;
}


// SVUOTA LIBRERIA
function svuota(){
    if(libreria.length != 0)
    {
        libreria.length = 0;
        mostraLibreria();
        alert("Libreria svuotata!");
    }
    else
    {
        alert("La libreria musicale è già vuota.")
    }
}

// RIMUOVI ELEMENTO
function rimuovi(var_id){
    for(let [index, song] of libreria.entries())
    {
        if(song.id == var_id)
        {
            libreria.splice(index, 1)
        } 
    };
    mostraLibreria();
}

// SVUOTA CAMPI INSERIMENTO
function svuotaCampi(){
    document.getElementById("inp_titolo").value = "";
    document.getElementById("inp_artista").value = "";
    document.getElementById("inp_anno").value = "";
}

// MOSTRA LIBRERIA
function mostraLibreria(){
    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
    let html_section = "";
    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
    for(let [index, song] of libreria.entries())
    {
        html_section += `
            <tr>
                <td>${song.titolo}</td>
                <td>${song.artista}</td>
                <td>${song.anno}</td>
                <td>
                    <button type="button" class="btn btn-outline-danger" onclick="rimuovi(${song.id})">
                        <i class="fa-solid fa-trash"></i>
                    </button>
                </td>
            </tr>
        `;
    }
    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
    document.getElementById("elenco-musicale").innerHTML = html_section;
    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
}

mostraLibreria();