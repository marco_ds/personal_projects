#pragma checksum "D:\Utenti\Marco\Desktop\CorsoOpenjobs\Bitbucket\Da caricare\Razor_Iscrizione_Corsi\Razor_Iscrizione_Corsi\Views\Corso\ElencoCorsi.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "df3dc71fa89397709bbedf47a77cf4be413567d2"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Corso_ElencoCorsi), @"mvc.1.0.view", @"/Views/Corso/ElencoCorsi.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"df3dc71fa89397709bbedf47a77cf4be413567d2", @"/Views/Corso/ElencoCorsi.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b853c2693020103458b52d8246f923ca3fd89014", @"/Views/_ViewImports.cshtml")]
    public class Views_Corso_ElencoCorsi : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<Razor_Iscrizione_Corsi.Models.Corso>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\n");
#nullable restore
#line 3 "D:\Utenti\Marco\Desktop\CorsoOpenjobs\Bitbucket\Da caricare\Razor_Iscrizione_Corsi\Razor_Iscrizione_Corsi\Views\Corso\ElencoCorsi.cshtml"
      
    Layout = "_Layout";
    

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
    <h1 class=""p-5 text-center text-light"">Lista Corsi</h1>

    <table class=""table table-striped text-center"" style=""background-color: rgba(255,255,255,0.9);"">
        <thead>
            <tr>
                <th>Codice</th>
                <th>Nome del corso</th>
                <th>Descrizione</th>
                <th>Inizio Corso</th>
            </tr>
        </thead>
        <tbody>
");
#nullable restore
#line 19 "D:\Utenti\Marco\Desktop\CorsoOpenjobs\Bitbucket\Da caricare\Razor_Iscrizione_Corsi\Razor_Iscrizione_Corsi\Views\Corso\ElencoCorsi.cshtml"
             foreach (var item in Model)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <tr>\n                <td>");
#nullable restore
#line 22 "D:\Utenti\Marco\Desktop\CorsoOpenjobs\Bitbucket\Da caricare\Razor_Iscrizione_Corsi\Razor_Iscrizione_Corsi\Views\Corso\ElencoCorsi.cshtml"
               Write(item.CodiceCorso);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\n                <td>");
#nullable restore
#line 23 "D:\Utenti\Marco\Desktop\CorsoOpenjobs\Bitbucket\Da caricare\Razor_Iscrizione_Corsi\Razor_Iscrizione_Corsi\Views\Corso\ElencoCorsi.cshtml"
               Write(item.TitoloCorso);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\n                <td>");
#nullable restore
#line 24 "D:\Utenti\Marco\Desktop\CorsoOpenjobs\Bitbucket\Da caricare\Razor_Iscrizione_Corsi\Razor_Iscrizione_Corsi\Views\Corso\ElencoCorsi.cshtml"
               Write(item.DescCorso);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\n                <td>");
#nullable restore
#line 25 "D:\Utenti\Marco\Desktop\CorsoOpenjobs\Bitbucket\Da caricare\Razor_Iscrizione_Corsi\Razor_Iscrizione_Corsi\Views\Corso\ElencoCorsi.cshtml"
               Write(item.DataCorso);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\n            </tr>\n");
#nullable restore
#line 27 "D:\Utenti\Marco\Desktop\CorsoOpenjobs\Bitbucket\Da caricare\Razor_Iscrizione_Corsi\Razor_Iscrizione_Corsi\Views\Corso\ElencoCorsi.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </tbody>\n    </table>\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<Razor_Iscrizione_Corsi.Models.Corso>> Html { get; private set; }
    }
}
#pragma warning restore 1591
