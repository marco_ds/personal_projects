﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Razor_Iscrizione_Corsi.Data;
using Razor_Iscrizione_Corsi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Razor_Iscrizione_Corsi.Controllers
{
    public class CorsoController : Controller
    {
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
        //                  SETUP                   //
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//

        private readonly InterfaceRepo<Corso> _repoCorso;

        public CorsoController(InterfaceRepo<Corso> corsoRepo)
        {
            _repoCorso = corsoRepo;
        }

        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
        //                 PAGINE                   //
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//

        public IActionResult ElencoCorsi()
        {
            List<Corso> listaCorsi = (List<Corso>)_repoCorso.ReadAll();

            ViewBag.Title = "Elenco Corsi";

            return View(listaCorsi);
        }

        public IActionResult Dettagli()
        {
            return View();
        }

        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
        //                FUNZIONI                  //
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//


    }
}
