﻿using Microsoft.AspNetCore.Mvc;
using Razor_Iscrizione_Corsi.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Razor_Iscrizione_Corsi.Data;

namespace Razor_Iscrizione_Corsi.Controllers
{
    public class HomeController : Controller
    {

        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
        //                 PAGINE                   //
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//

        public IActionResult Index()
        {
            return View();
        }

    }
}
