﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Razor_Iscrizione_Corsi.Data;
using Razor_Iscrizione_Corsi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Razor_Iscrizione_Corsi.Controllers
{
    public class UtenteController : Controller
    {
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
        //                  SETUP                   //
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//

        private readonly InterfaceRepo<Utente> _repoUtente;

        public UtenteController(InterfaceRepo<Utente> utenteRepo)
        {
            _repoUtente = utenteRepo;
        }

        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
        //                 PAGINE                   //
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//

        //public IActionResult ElencoUtenti()
        //{
        //    List<Utente> listaUtenti = (List<Utente>)_repoUtente.ReadAll();

        //    ViewBag.Title = "Elenco Utenti";

        //    return View(listaUtenti);
        //}

        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
        public IActionResult Login()
        {
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//

        public IActionResult ElencoUtenti()
        {
            List<Utente> listaUtenti = (List<Utente>)_repoUtente.ReadAll();

            ViewBag.Title = "Elenco Utenti";

            return View(listaUtenti);
        }

        public IActionResult AreaPersonale()
        {
            if (HttpContext.Session.GetString("isLogged") == null)
            {
                return Redirect("/Utente/Login");
            }
            else
            {
                return View();
            }
        }

        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
        //                FUNZIONI                  //
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//

        [HttpPost]
        public RedirectResult LoginCheck(Utente objUtente)
        {
            List<Utente> listaUtenti = (List<Utente>)_repoUtente.ReadAll();

            string username = objUtente.Username;
            string password = objUtente.Password;

            foreach (Utente item in listaUtenti)
            {
                if (item.Username.Equals(username) && item.Password.Equals(password))
                {
                    HttpContext.Session.SetString("isLogged", username);

                    return Redirect("/Home/Index");
                }
            }

            return Redirect("/Utente/Login");
        }

        [HttpPost]
        public IActionResult Inserisci(Utente objUtente)
        {
            if (ModelState.IsValid)
            {
                _repoUtente.Insert(objUtente);
                return Redirect("/Home/Index");
            }
            else
            {
                return View(objUtente);
            }
        }
    }
}
