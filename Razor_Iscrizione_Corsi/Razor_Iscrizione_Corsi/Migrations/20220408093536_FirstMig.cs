﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Razor_Iscrizione_Corsi.Migrations
{
    public partial class FirstMig : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Corsi",
                columns: table => new
                {
                    IdCorso = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TitoloCorso = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    DescCorso = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    CodiceCorso = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DataCorso = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Corsi", x => x.IdCorso);
                });

            migrationBuilder.CreateTable(
                name: "Utenti",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Cognome = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Indirizzo = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    DataNascita = table.Column<DateTime>(type: "datetime2", maxLength: 255, nullable: false),
                    Username = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Password = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Utenti", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Iscrizioni",
                columns: table => new
                {
                    IdIscrizione = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UtenteRef = table.Column<int>(type: "int", nullable: true),
                    CorsoRef = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Iscrizioni", x => x.IdIscrizione);
                    table.ForeignKey(
                        name: "FK_Iscrizioni_Corsi_CorsoRef",
                        column: x => x.CorsoRef,
                        principalTable: "Corsi",
                        principalColumn: "IdCorso",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Iscrizioni_Utenti_UtenteRef",
                        column: x => x.UtenteRef,
                        principalTable: "Utenti",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Iscrizioni_CorsoRef",
                table: "Iscrizioni",
                column: "CorsoRef");

            migrationBuilder.CreateIndex(
                name: "IX_Iscrizioni_UtenteRef",
                table: "Iscrizioni",
                column: "UtenteRef");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Iscrizioni");

            migrationBuilder.DropTable(
                name: "Corsi");

            migrationBuilder.DropTable(
                name: "Utenti");
        }
    }
}
