﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Razor_Iscrizione_Corsi.Models
{
    [Table("Utenti")]
    public class Utente
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(255)]
        [Required(ErrorMessage = "* Campo obbligatorio!")]
        public string Nome { get; set; }

        [MaxLength(255)]
        [Required(ErrorMessage = "* Campo obbligatorio!")]
        public string Cognome { get; set; }

        [MaxLength(255)]
        [Required(ErrorMessage = "* Campo obbligatorio!")]
        public string Indirizzo { get; set; }

        [MaxLength(255)]
        [Required(ErrorMessage = "* Campo obbligatorio!")]
        public DateTime DataNascita { get; set; }

        [MaxLength(255)]
        [Required(ErrorMessage = "* Serve un Username per accedere!")]
        public string Username { get; set; }

        [MaxLength(255)]
        [Required(ErrorMessage = "* Serve una Password per accedere!")]
        public string Password { get; set; }

        public ICollection<Iscrizione> Iscrizioni { get; set; }
    }
}
