﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Razor_Iscrizione_Corsi.Models
{
    [Table("Corsi")]
    public class Corso
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdCorso { get; set; }

        [MaxLength(255)]
        public string TitoloCorso { get; set; }

        [MaxLength(255)]
        public string DescCorso { get; set; }

        public string CodiceCorso { get; set; }

        public DateTime DataCorso { get; set; }

        public ICollection<Iscrizione> Iscritti { get; set; }

        // IDEA GuID -
        //public Corso()
        //{
        //    CodiceCorso = Guid.NewGuid().ToString();
        //}
    }
}
