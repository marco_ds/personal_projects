﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Razor_Iscrizione_Corsi.Models
{
    [Table("Iscrizioni")]
    public class Iscrizione
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdIscrizione { get; set; }

        [ForeignKey("UtenteRef")]
        public Utente Utente { get; set; }

        [ForeignKey("CorsoRef")]
        public Corso Corso { get; set; }
    }
}
