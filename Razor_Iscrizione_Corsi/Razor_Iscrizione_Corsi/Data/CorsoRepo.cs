﻿using Razor_Iscrizione_Corsi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Razor_Iscrizione_Corsi.Data
{
    public class CorsoRepo : InterfaceRepo<Corso>
    {
        private readonly DatabaseContext _context;

        public CorsoRepo(DatabaseContext context)
        {
            _context = context;
        }

        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
        //              BASIC CRUD                  //
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//

        // CREATE (NON MI SERVE)
        public Corso Insert(Corso t)
        {
            throw new NotImplementedException();
        }

        // READ
        public IEnumerable<Corso> ReadAll()
        {
            return _context.ElencoCorsi.ToList();
        }

        public Corso ReadById(int varId)
        {
            throw new NotImplementedException();
        }

        // UPDATE (NON MI SERVE)
        public bool Update(Corso t)
        {
            throw new NotImplementedException();
        }

        // DELETE (NON MI SERVE)
        public bool Delete(int varId)
        {
            throw new NotImplementedException();
        }

        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
        //                CUSTOM                    //
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//

        // operazioni

        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
    }
}
