﻿using Razor_Iscrizione_Corsi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Razor_Iscrizione_Corsi.Data
{
    public class UtenteRepo : InterfaceRepo<Utente>
    {
        private readonly DatabaseContext _context;

        public UtenteRepo(DatabaseContext context)
        {
            _context = context;
        }

        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
        //              BASIC CRUD                  //
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//

        // CREATE
        public Utente Insert(Utente t)
        {
            _context.ElencoUtenti.Add(t);
            _context.SaveChanges();

            return t;
        }

        // READ
        public IEnumerable<Utente> ReadAll()
        {
            return _context.ElencoUtenti.ToList();
        }

        public Utente ReadById(int varId)
        {
            throw new NotImplementedException();
        }

        // UPDATE
        public bool Update(Utente t)
        {
            throw new NotImplementedException();
        }

        // DELETE
        public bool Delete(int varId)
        {
            throw new NotImplementedException();
        }

        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
        //                CUSTOM                    //
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//

        // operazioni

        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
    }
}
