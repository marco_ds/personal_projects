﻿using Microsoft.EntityFrameworkCore;
using Razor_Iscrizione_Corsi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Razor_Iscrizione_Corsi.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> opt) : base(opt)
        {
        
        }

        public DbSet<Utente> ElencoUtenti { get; set; }

        public DbSet<Corso> ElencoCorsi { get; set; }
    }
}
