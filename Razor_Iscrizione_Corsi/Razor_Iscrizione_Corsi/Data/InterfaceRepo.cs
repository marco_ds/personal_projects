﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Razor_Iscrizione_Corsi.Data
{
    public interface InterfaceRepo<T>
    {
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
        //              BASIC CRUD                  //
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//

        // CREATE
        T Insert(T t);

        // READ
        IEnumerable<T> ReadAll();
        T ReadById(int varId);

        // UPDATE
        bool Update(T t);

        // DELETE
        bool Delete(int varId);
    }
}
