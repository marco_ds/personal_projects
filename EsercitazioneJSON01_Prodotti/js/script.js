let listaProdotti;

//#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
// UI - USER INTERFACE
//#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
$( document ).ready(function() {

  LoadJSON();

  $( "#btn_home" ).click(function() {
    HomeScreen();
    showProducts()
  });
  
  $( "#btn_abbigl" ).click(function() {
    TabScreen();
  });

  $( "#btn_calz" ).click(function() {
    TabScreen();
  });

  $( "#btn_borse" ).click(function() {
    TabScreen();
  });

  $( "#btn_uomo" ).click(function() {
    TabScreen();
  });

  $( "#btn_donna" ).click(function() {
    TabScreen();
  });

  $( "#btn_undthirty" ).click(function() {
    TabScreen();
  });

  $( "#btn_topfive" ).click(function() {
    TabScreen();
  });
  
});


//#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
// FUNCTIONS
//#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
function TabScreen()
{
  $( "#schermata1").hide();
  $( "#schermata2").show();
}

function HomeScreen()
{
  $( "#schermata1").show();
  $( "#schermata2").hide();
}

function LoadJSON()
{
  // JSON: http://www.giovannimastromatteo.it/projects/prodotti.json
  // obj with states. Make request, give response.
  const xhr = new XMLHttpRequest();

  xhr.onload = function()
  {
    if(this.status >= 200 && this.status < 300){
      try {
        listaProdotti = JSON.parse(this.responseText);
      } catch (e) {
        alert("Errore, ricontrollare il contenuto del JSON.")
      }
    } else {
      alert("errore nel caricamento del database")
    }
  };

  xhr.open('get', 'http://www.giovannimastromatteo.it/projects/prodotti.json');
  xhr.send();
}

function showProducts()
{
  let html_section = "";

  for(let [index, product] of listaProdotti.entries())
  {
    html_section += `
    <tr>
        <td>${product.categoria}</td>
        <td>${product.genere}</td>
        <td>${product.prodotto}</td>
        <td>${product.prezzo}</td>
        <td>${product.venduti}</td>
    </tr>`;
  }
  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
  document.getElementById("tab_products").innerHTML = html_section;
  //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
}