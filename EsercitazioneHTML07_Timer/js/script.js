let ore = 0;
let min = 0;
let sec = 0;
let intervallo;

function start(){
    reset();
    intervallo = setInterval(
        () => {
            sec++;
            if(sec == 60)
            {
                sec = 0;
                min++;
                if(min == 60)
                {
                    min = 0;
                    ore++;
                }
            }
            update(sec, min, ore);
        }, 125)
    
    function update(_s, _m, _h){
        let html_section = "";
        html_section = `${_s}`
        document.getElementById("sec").innerHTML = html_section;
        html_section = `${_m}`
        document.getElementById("min").innerHTML = html_section;
        html_section = `${_h}`
        document.getElementById("ore").innerHTML = html_section;
    }
}

function pause(){
    
}

function reset(){
    if(intervallo)
    {
        clearInterval(intervallo);
        document.getElementById("sec").innerHTML = `0`;
        document.getElementById("min").innerHTML = `0`;
        document.getElementById("ore").innerHTML = `0`;
    }
}

// //-------------
// //
// //-------------
// $(document).ready(
//     function(){

//         $("#btn-play").click()

//     }
// )